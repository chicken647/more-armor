# More Armor

This mod adds More Armor for Minecraft version 1.12.2 - 1.15.2 - 1.16.5


Check out all my mods: https://linktr.ee/chickenmods


Easy to obtain armor for all uses!  


+Custom Armor Attributes

+Custom Crafting Recipes


New: 

- Enhanced Netherite

- Powered Stick (Resource)

- Powered Ore (Resource)

- Powered Staff (Tool)

- Block Of Power 

- Power Nugget

- Power Dust


Armor Currently Added: 

- Emerald Armor

- Lapis Armor

- Redstone Armor

- Oak Armor

- Birch Armor

- Jungle Armor

- Acacia Armor

- Dark Oak Armor

- Leaves Armor

- Magma Armor

- Slime Armor

- Cobblestone Armor

- Ice Armor

- Golden Apple Armor

- Enchanted Golden Apple Armor

- Coal Armor

- Obsidian Armor

- Totem Armor

- Food Armor

- Glowing

- Water

- Sponge

- Tnt

- Shulker

- Enhanced Diamond

- Sugarcane

- Iron Bar

- Melon

- Dirt

- Sand

- Flint

- Sugar

- Stick


Want me to add a type of armor that is not currently available? Post a comment letting me know and ill see what I can do! 
